<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package lk4xone
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="wrapper">
		<div class="site-info">
            <div>
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'lk4xone' ) ); ?>">
				<?php
				/* translators: %s: CMS name, i.e. WordPress. */
				printf( esc_html__( 'Proudly powered by %s', 'lk4xone' ), 'WordPress' );
				?>
			</a>
            </div>
            <div>
				<?php
				/* translators: 1: Theme name, 2: Theme author. */
				printf( esc_html__( 'Theme: %1$s by %2$s.', 'lk4xone' ), 'lk4xone', '<a href="http://underscores.me/">Łukasz Kucharczyk</a>' );
				?>
                </div>
		</div><!-- .site-info -->
            </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
